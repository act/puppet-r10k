#
#fact can be set from env variable FACTER_puppet_env
$cron_env=$facts['puppet_env']

file { '/etc/cron.hourly/r10k':
  ensure => 'present',
  mode   => '0744',
  content => "#!/bin/bash
r10k deploy environment
cd `/opt/puppetlabs/bin/puppet config print environmentpath`/${cron_env}
r10k puppetfile install
/opt/puppetlabs/bin/puppet apply --environment ${cron_env} ./manifests
",
}
  
