Install puppet-agent from puppetlabs repository
Install r10k module

 sudo /opt/puppetlabs/bin/puppet module install puppet-r10k


To setup R10k run
  sudo FACTER_giturl=<git-repository-url> puppet apply r10k.pp
  
To update hiera config to user version 5 run
  sudo puppet apply hiera5.pp