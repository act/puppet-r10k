#!/bin/bash

usage ()
{
echo usage ./do-it-all.sh -r act/repo.git [-b production] [-s gitlab.act.reading.ac.uk] [-d deploy_key]
}

sudo /opt/puppetlabs/bin/puppet module install puppet-r10k 

GITREPO=""
BRANCH="production"
GITSERVER="gitlab.act.reading.ac.uk"
DEPLOYKEY=""

while [ "$1" != "" ]; do
    case $1 in
        -r | --repo )           shift
                                GITREPO=$1
                                ;;
        -b | --branch )         shift
                                BRANCH=$1
                                ;;
        -s | --server )         shift
                                GITSERVER=$1
                                ;;
        -d | --deploy_key )     shift
                                DEPLOYKEY=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$GITREPO" = "" ]; then
  usage
  exit 1
fi
PUPPETCMD=/opt/puppetlabs/bin/puppet 

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
#    echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
#    echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
#echo "SOURCE is '$SOURCE'"
RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
#if [ "$DIR" != "$RDIR" ]; then
#  echo "DIR '$RDIR' resolves to '$DIR'"
#fi

cd $DIR
sudo $PUPPETCMD apply hiera5.pp 

sudo FACTER_giturl=git@$GITSERVER:$GITREPO $PUPPETCMD apply r10k.pp 

sudo FACTER_puppet_env=$BRANCH $PUPPETCMD apply cron.pp 

sudo mkdir -m 700 /root/.ssh 

if [ "$DEPLOYKEY" = "" ]; then
  sudo ssh-keygen -t ed25519 -P "" -f /root/.ssh/r10k_deploy_key 
else
#check copied key
  sudo ssh-keygen -y -e -f $DEPLOYKEY
  if [ $? -eq 0 ]; then
    sudo cp $DEPLOYKEY /root/.ssh/r10k_deploy_key
    sudo chmod 0600 /root/.ssh/r10k_deploy_key
  else
    echo "deploy key not copied as it is invalid"
  fi
fi

cat << EOF | sudo tee -a /root/.ssh/config > /dev/null
Host $GITSERVER 
  IdentityFile /root/.ssh/r10k_deploy_key 
EOF

#Then we add the keys from the git server to root known_hosts file 

ssh-keyscan $GITSERVER | \
  sudo tee -a /root/.ssh/known_hosts > /dev/null 
chmod 600 /root/.ssh/known_hosts
